﻿using System;

namespace Eventer
{
    public class Publisher:IPublisher
    {
        public event EventHandler<MessageEventArgs> messagePublished;

        public void Run()
        {
            bool exit = false;
            do
            {
                string readLine = Console.ReadLine();
                if (readLine != null)
                {
                    messagePublished?.Invoke(this, new MessageEventArgs(readLine));
                }
                else
                {
                    exit = true;
                }
            } while (!exit);
        }
    }
}