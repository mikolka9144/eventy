﻿using System;

namespace Eventer
{
    public interface IPublisher
    {
        event EventHandler<MessageEventArgs> messagePublished;
    }
}