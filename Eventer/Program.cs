﻿using System;

namespace Eventer
{
    class Program
    {
        static void Main()
        {
            Publisher publisher = new Publisher();
            using (Subscriber subscriber = new Subscriber())
            {
                subscriber.RegisterToBublisher(publisher);

                var timerPublisher = new TimerPublisher();
                using (timerPublisher)
                {
                    subscriber.RegisterToBublisher(timerPublisher);
                    publisher.Run();
                }
            }
        }
    }
}
