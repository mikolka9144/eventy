﻿using System;
using System.Collections.Generic;

namespace Eventer
{
    public class Subscriber:IDisposable
    {
        private readonly List<IPublisher> _publishers = new List<IPublisher>();

        public void RegisterToBublisher(IPublisher publisher)
        {
            _publishers.Add(publisher);
            publisher.messagePublished += Publisher_messagePublished;
        }

        private void Publisher_messagePublished(object sender, MessageEventArgs e)
        {
            Console.WriteLine($"new Message: {e.Message} {DateTime.Now:T}");
        }
        
        public void Dispose()
        {
            foreach (var publisher in _publishers)
            {
                publisher.messagePublished -= Publisher_messagePublished;
            }
        }
    }
}