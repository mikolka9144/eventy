﻿using System;
using System.Linq;
using System.Timers;

namespace Eventer
{
    public class TimerPublisher:IPublisher,IDisposable
    {
        private readonly Timer _timer;

        public TimerPublisher()
        {
            _timer = new Timer()
            {
                Interval = 5000,
                Enabled = true
            };
            _timer.Elapsed += _timer_Elapsed;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            messagePublished?.Invoke(this,new MessageEventArgs("BOTER"));
        }

        private  EventHandler<MessageEventArgs> messagePublished;

        event EventHandler<MessageEventArgs> IPublisher.messagePublished
        {
            add
            {
                messagePublished += value;
                _timer.Enabled = true;
            }
            remove
            {
                messagePublished -= value;
                if (messagePublished == null || messagePublished.GetInvocationList().All(d => d == messagePublished))
                {
                    _timer.Enabled = false;
                }
            }
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}